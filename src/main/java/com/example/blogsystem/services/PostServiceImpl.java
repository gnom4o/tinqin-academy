package com.example.blogsystem.services;

import com.example.blogsystem.models.Post;
import com.example.blogsystem.repositories.CommentRepository;
import com.example.blogsystem.repositories.PostRepository;
import com.example.blogsystem.services.contracts.PostService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository repository;
    private final CommentRepository commentRepository;


    @Override
    public void create(Post post) {
        repository.saveAndFlush(post);
    }

    @Override
    public void update(int id,Post post) {
        Post currentPost = repository.findById(id).orElseThrow(() -> new NoSuchElementException("Invalid post id"));
        currentPost.setContent(post.getContent());
        repository.saveAndFlush(currentPost);
    }

    @Override
    public void delete(Post post) {

        if(!post.getComments().isEmpty()){

            commentRepository.deleteAll(post.getComments());

        }
        repository.delete(post);
    }

    @Override
    public List<Post> getAllPosts() {
       return repository.findAll();
    }

    @Override
    public Post getPost(int id) {
        return repository.findById(id).orElseThrow(()-> new NoSuchElementException("Invalid post id"));
    }
}
