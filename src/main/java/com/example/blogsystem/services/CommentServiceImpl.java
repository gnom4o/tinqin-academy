package com.example.blogsystem.services;

import com.example.blogsystem.models.Comment;
import com.example.blogsystem.models.Post;
import com.example.blogsystem.repositories.CommentRepository;
import com.example.blogsystem.services.contracts.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Override
    public void create(Comment comment, Post post) {
        comment.setPost(post);
        commentRepository.saveAndFlush(comment);

    }

    @Override
    public void update(int id, Comment comment) {
        Comment currentComment = commentRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Invalid comment id"));
        currentComment.setContent(comment.getContent());
        commentRepository.saveAndFlush(currentComment);
    }

    @Override
    public void delete(Comment comment) {
        commentRepository.delete(comment);
    }

    @Override
    public Comment getComment(int id) {
        return commentRepository.findById(id).orElseThrow(()-> new NoSuchElementException("Invalid comment id"));
    }
}
