package com.example.blogsystem.services.contracts;

import com.example.blogsystem.models.Comment;
import com.example.blogsystem.models.Post;



public interface CommentService {

    void create (Comment comment, Post post);
    void update (int id,Comment comment);
    void delete (Comment comment);
    Comment getComment(int id);
}
