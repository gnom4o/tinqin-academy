package com.example.blogsystem.services.contracts;

import com.example.blogsystem.models.Post;

import java.util.List;

public interface PostService {
    void create (Post post);
    void update (int id,Post post);
    void delete (Post post);

    List<Post> getAllPosts();
    Post getPost(int id);
}
