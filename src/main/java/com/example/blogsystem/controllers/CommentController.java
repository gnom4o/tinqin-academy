package com.example.blogsystem.controllers;

import com.example.blogsystem.models.Comment;
import com.example.blogsystem.models.dtos.CommentDto;
import com.example.blogsystem.models.mappers.CommentDtoMapper;
import com.example.blogsystem.services.contracts.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentDtoMapper commentDtoMapper;

    @GetMapping("{id}")
    public Comment getComment(@PathVariable int id){
        return commentService.getComment(id);
    }
    @PutMapping("{id}")
    public void updateComment(@PathVariable int id, @RequestBody CommentDto commentDto){
        try {
            Comment comment = commentDtoMapper.commentDtoToObject(commentDto);
            commentService.update(id,comment);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @DeleteMapping("{id}")
    public void deleteComment(@PathVariable int id){
        try {
            Comment comment = commentService.getComment(id);
            commentService.delete(comment);
        }catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
