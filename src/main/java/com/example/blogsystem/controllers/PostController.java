package com.example.blogsystem.controllers;

import com.example.blogsystem.models.Comment;
import com.example.blogsystem.models.Post;
import com.example.blogsystem.models.dtos.CommentDto;
import com.example.blogsystem.models.dtos.PostDto;
import com.example.blogsystem.models.mappers.CommentDtoMapper;
import com.example.blogsystem.models.mappers.PostDtoMapper;
import com.example.blogsystem.services.contracts.CommentService;
import com.example.blogsystem.services.contracts.PostService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@AllArgsConstructor
@RequestMapping("/api/posts")
public class PostController {

    private final PostService postService;
    private final PostDtoMapper postDtoMapper;
    private final CommentService commentService;
    private final CommentDtoMapper commentDtoMapper;
    @GetMapping
    public List<Post> getAll(){
        return postService.getAllPosts();
    }
    @PostMapping()
    public void createPost(@RequestBody @Valid PostDto postDto ) {
        postService.create(postDtoMapper.postDtoToObject(postDto));
    }
    @GetMapping("/{id}")
    public Post getPostById(@PathVariable int id){
        try {
            return postService.getPost(id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }
    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable int id) {
        try {
            Post post = postService.getPost(id);
            postService.delete(post);
        }catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }
    @PutMapping("/{id}")
    public void updatePost(@PathVariable int id, @RequestBody PostDto postDto) {
        try {
            postService.update(id,postDtoMapper.postDtoToObject(postDto));
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PostMapping("/{id}/comment")
    public void addComment(@PathVariable int id, @RequestBody @Valid CommentDto commentDto){
        try {
            Post post = postService.getPost(id);
            Comment comment = commentDtoMapper.commentDtoToObject(commentDto);
            commentService.create(comment,post);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
