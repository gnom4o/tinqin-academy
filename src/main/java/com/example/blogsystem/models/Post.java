package com.example.blogsystem.models;




import jakarta.persistence.*;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="posts")
@Data
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int postId;

    @Column(name = "content")
    private String content;

    @Column(name= "created_on")
    private LocalDateTime created;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "post")

    private List<Comment> comments;
}
