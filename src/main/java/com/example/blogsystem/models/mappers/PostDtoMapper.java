package com.example.blogsystem.models.mappers;

import com.example.blogsystem.models.Post;
import com.example.blogsystem.models.dtos.PostDto;
import com.example.blogsystem.services.contracts.PostService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
@AllArgsConstructor
public class PostDtoMapper {
   PostService postService;

   public Post postDtoToObject(PostDto postDto){
       Post post = new Post();
       post.setContent(postDto.getPost());

       post.setCreated(LocalDateTime.now());
       return post;
   }
}
