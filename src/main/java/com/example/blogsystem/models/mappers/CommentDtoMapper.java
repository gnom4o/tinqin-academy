package com.example.blogsystem.models.mappers;

import com.example.blogsystem.models.Comment;
import com.example.blogsystem.models.dtos.CommentDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class CommentDtoMapper {

    public Comment commentDtoToObject(CommentDto commentDto){
        Comment comment = new Comment();
        comment.setContent(commentDto.getComment());

        comment.setCreated(LocalDateTime.now());
        return comment;
    }
}
