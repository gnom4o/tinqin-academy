package com.example.blogsystem.models.dtos;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;


@Data

public class CommentDto {
    @NotEmpty
   private String comment;
}
